/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugins.barcode;

import java.util.UUID;

import org.ambientdynamix.api.contextplugin.ContextPluginRuntime;
import org.ambientdynamix.api.contextplugin.ContextPluginSettings;
import org.ambientdynamix.api.contextplugin.IPluginView;
import org.ambientdynamix.api.contextplugin.PowerScheme;

import android.util.Log;

import com.google.zxing.client.android.CaptureActivity;

// See: http://code.google.com/p/zxing/
public class BarcodeContextPluginRuntime extends ContextPluginRuntime {
	private final String TAG = this.getClass().getSimpleName();
	private boolean _done;
	private boolean runOnce;
	private PowerScheme powerScheme;

	@Override
	public void setPowerScheme(PowerScheme scheme) {
		Log.d(TAG, "Setting PowerScheme " + scheme);
	}

	@Override
	public void init(PowerScheme scheme, ContextPluginSettings settings) throws Exception {
		this.setPowerScheme(scheme);
	}

	@Override
	public void updateSettings(ContextPluginSettings settings) {
		// TODO Auto-generated method stub
	}

	@Override
	public void start() {
		Log.d(TAG, "Started!");
	}

	@Override
	public void stop() {
		Log.d(TAG, "Stopped!");
	}

	@Override
	public void destroy() {
		stop();
		Log.d(TAG, "Destroyed!");
	}

	public void scanToInteractFeature() {
		Log.i(TAG, "scanToInteractFeature!");
		openView(new CaptureActivity(getSecuredContext(), this, null,
				"org.ambientdynamix.contextplugins.barcode.features.scantointeract"));
	}
}